# BaseBrain

BaseBrain act as an extension from laravel, a jump start to build laravel application, main power of basebrain come from modular-mulit-role user management, it can handle user and policies easily, and can be easily expanded with other project that use BaseBrain.

## Getting Started

These instructions will guide you through deployment of this project, and the you will be ready to build your application without worrying about user management. 

Reading this you acknowledge that you already familiar with laravel,  especially handling controller, model, dependency injection and policies.

### Prerequisites
Below is what you need to deploy.
- Composer
- PHP 7
- PostgreSQL (preferred)

### Installing
Installing is fairly easy
1. Clone this repo
2. Run ```composer install```
3. Run ```php artisan migrate```
3. Edit your own ```.env``` and some config files you need (config/app for example)
4. Please check wiki [here](https://gitlab.com/fikrimi23/basebrain/wikis/understanding-concept)


## Built With

* [Laravel 5.4](https://laravel.com/docs/5.4) - Backend Framework
* [Composer](https://getcomposer.org/doc/) - Dependency Manager
* [Bootstrap 3](http://getbootstrap.com) - Front End Framework
* [Gentalella Alela](https://colorlib.com/polygon/gentelella/index.html) - Template used

## Acknowledgments

* Thanks to Mr Ikbal who guided me to the world of laravel and this concept is basically his with some refinement from me.
* In indexing progress for plugin used