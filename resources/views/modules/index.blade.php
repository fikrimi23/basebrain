@extends('layouts.dashboard')

@section('css')
    {!! Layouts::usePlugin('datatables', 'css') !!}
@endsection

@section('breadcrumbs')
<li class="breadcrumb-item"><a href="{{ route('modules.index') }}">Modules</a></li>
@endsection

@section('content')
    @component('layouts.components.datatables')
        @slot('title')
            @choice('rolegroups.title', 2)
        @endslot

        @slot('topButton')
            @can('create', 'App\Module')
                <a href="{{ route('modules.create') }}" class="btn btn-primary btn-md pull-right">
                    <i class="fa fa-fw fa-user-plus"></i> New @choice('modules.title', 1)
                </a>
            @endcan
        @endslot

        @slot('table')
            {!! $html->table(['style' => 'width:100%;']) !!}
        @endslot

    @endcomponent
@endsection

@push('js')
    {!! Layouts::usePlugin('datatables', 'js') !!}
    {!! $html->scripts() !!}
@endpush
