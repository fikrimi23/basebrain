@extends('layouts.dashboard')

@section('title', 'PageTitle')

@section('breadcrumbs')
<li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <h4>
                <span class="text-uppercase">@lang('dashboard.title')</span>
            </h4>
        </div>
    </div>
</section>
@endsection
