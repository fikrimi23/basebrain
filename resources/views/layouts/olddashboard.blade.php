<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php
    // echo $this->template->title;
    ?></title>

    <?php
        // echo $this->template->meta;
        // echo $this->template->stylesheet;
        // echo $this->template->favicon;
        ?>
</head>
<body class="flat-green">
    <div class="app-container">
        <div class="row content-container">
            <!-- Navigation Header -->
            <nav class="navbar navbar-default navbar-fixed-top navbar-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-expand-toggle">
                            <i class="fa fa-bars icon"></i>
                        </button>
                        <ol class="breadcrumb navbar-breadcrumb hidden-xs">
                        <?php if (isset($breadcrumbs)) : ?>
                            <?php foreach ($breadcrumbs as $key => $breadcrumb) : ?>
                                <li>{{-- <?php echo $breadcrumb; ?> --}}</li>
                            <?php endforeach ?>
                        <?php endif ?>
                        </ol>
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-th icon"></i>
                        </button>
                    </div>
                    <!-- /.navbar-header -->
                    <ul class="nav navbar-nav navbar-right">
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>
                        <li class="dropdown profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{-- <?php echo $this->session->profile->nama; ?> <span class="caret"></span> --}}
                            </a>
                            <ul class="dropdown-menu animated fadeInDown">
                                <li>
                                    <div class="profile-info">
                                        <h4 class="username">{{-- <?php echo $this->session->profile->nama; ?> --}}</h4>
                                        <p><!-- <?php echo $this->session->email; ?> --></p>
                                        <div class="btn-group margin-bottom-2x" role="group">
                                            <a href="{{-- <?php echo base_url('auth/logout'); ?> --}}" type="button" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</a>
                                        </div>
                                        <!-- /.btn-group -->
                                    </div>
                                    <!-- /.profile-info -->
                                </li>
                            </ul>
                            <!-- /.dropdown-menu -->
                        </li>
                        <!-- /.dropdown -->
                    </ul>
                    <!-- /.nav -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <!-- /.navbar -->

            <!-- Sidebar Menu -->
            <?php echo $this->template->sidebar; ?>

            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <?php foreach ($this->session->messages as $message) : ?>
                        <div class="alert fresh-color alert-{{-- <?php echo $message['severity_color']; ?> --}} ">
                            <b>{{-- <?php echo $message['severity_message']; ?> --}}</b> {{-- <?php echo $message['message_body']; ?> --}}
                        </div>
                    <?php endforeach ?>
                    <?php unset($_SESSION['messages']); ?>
                    <div class="page-title">
                        <span class="title">{{-- <?php echo $this->template->title; ?> --}}</span>
                        <div class="description">{{-- <?php echo $this->template->subtitle; ?> --}}</div>
                    </div>
                    {{-- <?php echo $this->template->content; ?> --}}
                </div>
                <!-- /.side-body -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-container -->
    </div>
    <!-- /.app-container -->
    <?php
        // echo $this->template->javascript;
        ?>
</body>
</html>
