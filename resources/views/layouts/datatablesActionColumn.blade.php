{{-- Button if url view is exist --}}
@if (isset($view))
<a class="m-l-5 m-r-sm" href="{{ $view }}" data-toggle="tooltip" data-placement="bottom" title="View">
    <i class="fa fa-lg fa-eye"></i>
</a>
@endif

{{-- Button if url edit is exist --}}
@if (isset($edit))
<a class="m-l-5 m-r-sm" href="{{ $edit }}" data-toggle="tooltip" data-placement="bottom" title="Edit">
    <i class="fa fa-lg fa-pencil"></i>
</a>
@endif

{{-- Button if url delete is exist --}}
@if (isset($delete))
{!! Form::open([ 'url' => $delete, 'method'=>'delete', 'class'=>'form-inline','style="display:inline;"' ]) !!}
    <a href="#" data-confirm="Are you sure to delete this resources?" class="m-l-5 m-r-sm js-delete-confirm" data-toggle="tooltip" data-placement="bottom" title="Delete">
        <i class="fa fa-lg fa-trash"></i>
    </a>
{!! Form::close() !!}
@endif
