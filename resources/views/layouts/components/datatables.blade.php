<div class="page-title">
    <div class="title_left">
        <h3>{{ $title }}</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{ $title }} <small class="text-lowercase">show all {{ $title }}</small></h2>
                {{ $topButton or '' }}
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                {{ $table }}
            </div>
        </div>
    </div>
</div>
