<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Route;
use App\Role;

class SingleObjectComposer
{

    /**
     * Return user for easy access to user
     *
     * @return App\User
     */
    // public function user()
    // {
    //     $user = auth()->user();
    //     return $user;
    // }

    /**
     * Return isEdit for easy access if edit needed
     *
     * @return boolean
     */
    public function isEdit()
    {
        $isEdit = (explode('.', Route::currentRouteName())[1] ?? false) === 'edit';

        return $isEdit;
    }

    public function compose(View $view)
    {
        // $view->share('user', $this->user());
        $view->with('isEdit', $this->isEdit());
    }
}
