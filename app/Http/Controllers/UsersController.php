<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Staff;
use App\Rolegroup;
use App\Http\Requests\UpdateUserRequest;

use App\Dependencies\CustomBuilder;

use DB;
use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomBuilder $builder)
    {
        $html = $builder
            ->parameters()
            ->ajax([
                'url' => route('datatables.users'),
            ])
            ->columns([
                ['data' => 'email',     'title' => trans('datatables.header.email')],
                ['data' => 'is_active', 'title' => trans('datatables.header.active')],
            ])
            ->addAction();
        return view('users.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new users.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staffs = Staff::where(DB::raw('user_id IS NOT NULL'))->get();
        $rolegroups = Rolegroup::notSuper()->get();
        return view('users.form')->with([
            'staffs' => ['' => 'Create New Profile'], $staffs->pluck('name', 'id')->toArray(),
            'rolegroups' => $rolegroups->pluck('rolegroup_name', 'id'),
        ]);
    }

    /**
     * Store a newly created users in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateUserRequest $request)
    {
        DB::transaction(function () use ($request) {
            // create new User
            $user = new User();
            $user->fill($request->only(['email', 'password']));
            $user->rolegroup_id = $request->input('rolegroup.id');
            $user->password = Hash::make($user->password);
            $user->save();

            // create new staff and associate to already created user
            $staff = new Staff($request->input('staff'));
            $staff->user()->associate($user);
            $staff->save();
        });
        return redirect()->route('users.index');
    }

    /**
     * Display the specified users.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // return view('users.show')->with('module', $user);
    }

    /**
     * Show the form for editing the specified users.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user->load(['rolegroup', 'staff']);

        $staffs = Staff::where(DB::raw('user_id IS NOT NULL'))->get();
        $rolegroups = Rolegroup::notSuper()->get();
        return view('users.form')->with([
            'user' => $user,
            'staffs' => ['' => 'Create New Profile'], $staffs->pluck('name', 'id')->toArray(),
            'rolegroups' => $rolegroups->pluck('rolegroup_name', 'id'),
            ]);
    }

    /**
     * Update the specified users in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user = DB::transaction(function () use ($request, $user) {
            // autofill user
            $user->fill($request->only('email', 'password'));

            // check for password
            if (empty($user->password)) {
                unset($user->password);
            } else {
                $user->password = Hash::make($user->password);
            }

            // fill relation id
            if ($user->rolegroup->rolegroup_depth > 0) {
                $user->rolegroup_id = $request->input('rolegroup.id');
            }
            $user->save();

            // update child
            $user->staff->update($request->input('staff'));
            return $user;
        });
        return redirect()->route('users.edit', $user->id);
    }

    /**
     * Remove the specified users from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        DB::transaction(function () use ($user) {
            $user->delete();
        });
        return redirect()->route('users.index');
    }
}
