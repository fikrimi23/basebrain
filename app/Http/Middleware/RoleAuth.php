<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Route;
use \App\Role;

class RoleAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module)
    {
        // 1st validation, check for routename
        $routeName = explode('.', Route::currentRouteName());

        $user = auth()->user();

         // as using resource, theres 7 routename assigned
        switch (end($routeName)) {
            case 'index':
            case 'show':
                $status = $user->can('view', $module);
                break;
            case 'edit':
            case 'update':
                $status = $user->can('update', $module);
                break;
            case 'create':
            case 'store':
                $status = $user->can('create', $module);
                break;
            case 'destroy':
                $status = $user->can('delete', $module);
                break;
            default:
                // if not sing resource use special cases
                switch (first($routeName)) {
                    case 'datatables':
                        $status = $user->can('view', $module);
                        break;
                    default:
                        $status = false;
                        break;
                }
                break;
        }

        // check for status
        if ($status) {
            return $next($request);
        } else {
            // if ajax return abort, if not redirect with error messages
            if ($request->ajax()) {
                return abort(401, trans('auth.unauthorized'));
            } else {
                \Flash::push(
                    'error-message',
                    trans('auth.unauthorized')
                );
                return back();
            }
        }

        return $next($request);
    }
}
