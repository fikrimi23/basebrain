<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

use Auth;
use App\Role;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            'layouts.dashboard',
            'App\Http\ViewComposers\NavbarComposer'
        );

        view()->composer(
            ['*.form', '*.show'],
            'App\Http\ViewComposers\SingleObjectComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
