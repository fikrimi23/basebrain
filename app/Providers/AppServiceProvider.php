<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Module;
use App\Observer\ModuleObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Module::observe(ModuleObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
