<?php

function ddr()
{
    $args = func_get_args();
    foreach ($args as $key => $arg) {
        if ($arg instanceof Illuminate\Contracts\Support\Arrayable) {
            $args[$key] = $arg->toArray();
        }
    }
    call_user_func_array('dd', $args);
}

function getInitials($stmt)
{
    $words = explode(" ", $stmt);
    $acronym = "";

    foreach ($words as $w) {
        $acronym .= $w[0];
    }

    return $acronym;
}
