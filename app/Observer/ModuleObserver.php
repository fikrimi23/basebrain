<?php

namespace App\Observer;

use App\Module;
use App\Helper\Flash;
use Illuminate\Http\Exceptions;

class ModuleObserver
{
    public function deleting(Module $module)
    {
        if ($module->locked) {
            Flash::push('error-message', trans('modules.delete_locked'));
            return false;
        } else {
            return true;
        }
    }
}
