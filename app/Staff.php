<?php

namespace App;

use App\BaseModel as Model;

class Staff extends Model
{
    protected $table = 'staffs';

    protected $fillable = [
        'name', 'user_id'
    ];

    public $moduleName = 'Staff';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
