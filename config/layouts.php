<?php

return [
    'plugins' => [
        'css' => [
            'select2' => [
                'vendor/select2/css/select2.min.css',
                'vendor/select2-bootstrap-theme/css/select2-bootstrap.min.css',
            ],
            'datatables' => [
                'vendor/datatables/datatables.min.css',
                'vendor/datatables/DataTables-1.10.13/css/dataTables.bootstrap.min.css',
            ],
            'fullcalendar' => 'vendor/fullcalendar/css/fullcalendar.min.css',
            'pnotify' => 'vendor/pnotify/css/pnotify.min.css',
            'nprogress' => 'vendor/nprogress/css/nprogress.css',
        ],
        'js' => [
            'select2' => 'vendor/select2/js/select2.full.min.js',
            'parsley' => 'vendor/parsley/parsley.min.js',
            'datatables' => [
                'vendor/datatables/DataTables-1.10.13/js/jquery.dataTables.min.js',
                'vendor/datatables/DataTables-1.10.13/js/dataTables.bootstrap.min.js',
            ],
            'fullcalendar' => 'vendor/fullcalendar/js/fullcalendar.min.js',
            'moment' => 'vendor/moment/moment.min.js',
            'jqueryui' => 'vendor/jquery-ui/jquery-ui.min.js',
            'pnotify' => 'vendor/pnotify/js/pnotify.min.js',
            'nprogress' => 'vendor/nprogress/js/nprogress.js',
        ]
    ]
];
