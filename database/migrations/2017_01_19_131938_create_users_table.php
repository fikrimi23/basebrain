<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('rolegroup_id');
            $table->string('email')->unique();
            $table->string('password', 64);
            $table->boolean('is_active');
            $table->string('activation_code')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->primary('id');

            $table->index('rolegroup_id', 'users_indexes');
            $table->foreign('rolegroup_id')->references('id')->on('rolegroups')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
