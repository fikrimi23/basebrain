<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('file_name')->unique();
            $table->string('client_file_name');
            $table->string('extension', 5)->index();
            $table->integer('size')->unsigned();
            $table->string('mime');
            $table->timestamps();

            $table->primary('id');

            $table->uuid('upload_by')->nullable();
            $table->foreign('upload_by')->references('id')->on('users')
                  ->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
