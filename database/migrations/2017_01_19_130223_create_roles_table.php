<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('rolegroup_id');
            $table->uuid('module_id');
            $table->string('role_ability', 7);
            $table->timestamps();

            $table->index(['rolegroup_id', 'module_id'], 'role_indexes');

            $table->primary('id');

            $table->foreign('rolegroup_id')->references('id')->on('rolegroups')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('module_id')->references('id')->on('modules')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
